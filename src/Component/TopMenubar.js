import React, { Component } from 'react'
import Performance from './Dashboard/Performance'
import Status from './Dashboard/Status'
import Process from './Dashboard/Process'
import Timeline from './Dashboard/Timeline'
import TaskList from './Dashboard/TaskList'
import LineChart from './Chart/LineChart'
import BarChart from './Chart/BarChart'
import CardGroup from './Dashboard/CardGroup'
import Footer from './Dashboard/Footer'
import './style.css'
import IncidentList from './Dashboard/IncidentList'

export default class TopMenubar extends Component {
    render() {
        return (
            <div>
                <header>
                    <div class="container">
                        <nav>
                            <div class="logo">
                                <h1>MySite</h1>
                            </div>
                            <ul class="menu">
                                <li><a href="# ">Menu 1</a></li>
                                <li>
                                    <div class="dropdown">
                                        Menu 2
                                        <div class="dropdown-content">
                                            <a href="# ">Link 1</a>
                                            <a href="# ">Link 2</a>
                                            <a href="# ">Link 3</a>
                                        </div>
                                    </div>
                                </li>
                                <li><a href="# ">Menu 3</a></li>
                                <li><a href="# ">Menu 4</a></li>
                                <li><a href="# ">Menu 5</a></li>
                                <li><a href="# ">Menu 6</a></li>
                            </ul>
                        </nav>
                    </div>
                </header>
                <section>
                    <div className="container">
                        <div className="double-column">
                            <LineChart />
                            <CardGroup />
                        </div>
                        <IncidentList />
                        <Status />
                        <div className="double-column">
                            <LineChart />
                            <BarChart />
                        </div>
                        {/* <TaskList /> */}
                        {/* <Footer /> */}
                    </div>
                </section>
            </div>
        )
    }
}
