import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Drawer, CssBaseline, AppBar, Toolbar, Typography, Divider, IconButton, Button, Tabs, Tab } from '@material-ui/core';
import { Menu, Dehaze, ChevronLeft, ChevronRight } from '@material-ui/icons';
import ListItemMenu from './ListItemMenu'
import Config from './MenuConfig'
import Performance from './Dashboard/Performance'
import Status from './Dashboard/Status'
import Process from './Dashboard/Process'
import Timeline from './Dashboard/Timeline'
import TaskList from './Dashboard/TaskList'
import Footer from './Dashboard/Footer'
import './style.css'

const drawerWidth = 300;
const containerWidth = 1140;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
    tabList: {
        justifyContent: "flex-end"
    }
}));

export default function SideLeftBar() {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const [state, setState] = useState({
        mainTitle: '',
        subTitle: ''
    });

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    function handleChange(name, value) {
        setState({
            [name]: value
        });
        console.log(name, value)
    }

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <Menu />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <Dehaze /> : <Dehaze />}
                    </IconButton>
                </div>
                <Divider />
                <ListItemMenu mainTitle={state.mainTitle} subTitle={state.subTitle} primary="Dashboard" subList="subTitle" />
                <ListItemMenu />
            </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                })}
            >
                <div className={classes.drawerHeader} />
                <div className={classes.container}>
                    {/* <Config mainTitle={state.mainTitle} subTitle={state.subTitle} onChange={handleChange} /> */}
                    <Performance />
                    <Status />
                    <div className="double-column">
                        <Process />
                        <Timeline />
                    </div>
                    {/* <TaskList /> */}
                    <Footer />
                </div>
            </main>
        </div>
    );
}
