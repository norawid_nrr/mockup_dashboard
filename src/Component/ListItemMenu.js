import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemIcon, ListItemText, Collapse, ListSubheader } from '@material-ui/core';
import { Inbox, Drafts, Pages, ExpandLess, ExpandMore, StarBorder, Dashboard } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    nestedList: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(6),
        borderLeft: '3px solid lightblue',
        left: '15%'
    },
    subHeader: {
        textTransform: 'uppercase',
        fontSize: '.85rem',
        fontWeight: 'bold',
        color: '#033C73',
        whiteSpace: 'nowrap',
        position: 'relative',
    },
    listItemIcon: {
        paddingLeft: theme.spacing(2),
    },
    lightWeight: {
        opacity: '0.5'
    },
    listItemText: {
        fontSize: '0.8rem',
    },
    listItemTextSub: {
        fontSize: '0.8rem',
    }
}));

export default function ListItemMenu({ mainTitle, subTitle, primary, subList }) {
    const classes = useStyles();
    const [openSub, toggleSub] = React.useState(false);

    const handleClick = () => {
        toggleSub(!openSub);
    };

    return (
        <div>
            <List
                component="div"
                aria-labelledby="nested-list-subheader"
                subheader={
                    <ListSubheader component="div" id="nested-list-subheader" className={classes.subHeader}>
                        Main Navigation
                    </ListSubheader>
                }
                className={classes.nestedList}
            >
                <ListItem button className={classes.listItem}>
                    <ListItemIcon className={`${classes.listItemIcon} ${classes.lightWeight}`}>
                        <Dashboard />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.listItemText }} primary={primary} />
                </ListItem>
                <ListItem button className={classes.listItem}>
                    <ListItemIcon className={`${classes.listItemIcon} ${classes.lightWeight}`}>
                        <Pages />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.listItemText }} primary="Pages" />
                </ListItem>
                <ListItem button onClick={handleClick} className={classes.listItem}>
                    <ListItemIcon className={`${classes.listItemIcon} ${classes.lightWeight}`}>
                        <Inbox />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.listItemText }} primary="Inbox" />
                    {openSub ? <ExpandLess className={classes.lightWeight} /> : <ExpandMore className={classes.lightWeight} />}
                </ListItem>
                <Collapse in={openSub} timeout="auto" unmountOnExit className={classes.collape}>
                    <List component="div" disablePadding>
                        <ListItem button className={classes.nested}>
                            <ListItemText classes={{ primary: classes.listItemTextSub }} primary={subList} />
                        </ListItem>
                        <ListItem button className={classes.nested}>
                            <ListItemText classes={{ primary: classes.listItemTextSub }} primary={subList} />
                        </ListItem>
                    </List>
                </Collapse>
                <ListItem button className={classes.listItem}>
                    <ListItemIcon className={`${classes.listItemIcon} ${classes.lightWeight}`}>
                        <Pages />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.listItemText }} primary="Pages" />
                </ListItem>
            </List>
        </div >
    );
}