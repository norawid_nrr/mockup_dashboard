import React, { Component } from 'react'

export default class Performance extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-header">
                    Number of Incidents
                </div>
                <div className="card-content">
                    <div className="widget-wrapper">
                        <div className="icon-wrapper">
                            <div className="icon-box">
                                <i class="fa fa-window-close danger"></i>
                            </div>
                        </div>
                        <div className="detail-widget">
                            <div className="detail-title">Cash Deposits</div>
                            <div className="detail-number">1,7M</div>
                            <div className="detail-rate">
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <div className="rating">54.1%</div>
                                <div className="rate-text purple">less earnings</div>
                            </div>
                        </div>
                    </div>
                    <div className="widget-wrapper">
                        <div className="icon-wrapper">
                            <div className="icon-box">
                                <i class="fa fa-exclamation-triangle warning"></i>
                            </div>
                        </div>
                        <div className="detail-widget">
                            <div className="detail-title">Invested Dividents</div>
                            <div className="detail-number">9M</div>
                            <div className="detail-rate">
                                <div className="rate-text purple">Grow Rate: </div>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <div className="rating">14.1%</div>
                            </div>
                        </div>
                    </div>
                    <div className="widget-wrapper">
                        <div className="icon-wrapper">
                            <div className="icon-box">
                                <i class="fa fa-check success"></i>
                            </div>
                        </div>
                        <div className="detail-widget">
                            <div className="detail-title">Capital Gains</div>
                            <div className="detail-number">$563</div>
                            <div className="detail-rate">
                                <div className="rate-text">Increased by</div>
                                <i class="fa fa-angle-up" aria-hidden="true"></i>
                                <div className="rating">7.35%</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-footer center">
                    <button>
                        View Complete Report
                    </button>
                </div>
            </div>
        )
    }
}
