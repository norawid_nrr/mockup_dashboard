import React, { Component } from 'react'

export default class Status extends Component {
    render() {
        return (
            <div>
                <div className="card">
                    <div className="card-header">
                        Working Agent Status
                    </div>
                    <div className="card-content">
                        <div className=" table-main">
                            <div className="table-wrapper">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Avatar</th>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Status</th>
                                            <th>Due Date</th>
                                            <th>Target Achievement</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>#54</td>
                                            <td>
                                                <div className="avatar center">
                                                    <img src="https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="" />
                                                </div>
                                            </td>
                                            <td>
                                                <a href="# ">Juan C. Cargill</a>
                                            </td>
                                            <td>
                                                Developer
                                            </td>
                                            <td>
                                                <div className="status">
                                                    Canceled
                                                </div>
                                            </td>
                                            <td>
                                                <div className="date">
                                                    12 Dec
                                                </div>
                                            </td>
                                            <td>
                                                <div className="achive-process">
                                                    <div className="process-percent">71%</div>
                                                    <div className="process-bar">
                                                        <div class="process" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="action">
                                                    <button className="hire">Detail</button>
                                                    {/* <button className="fire">Fire</button> */}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#54</td>
                                            <td>
                                                <div className="avatar center">
                                                    <img src="https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="" />
                                                </div>
                                            </td>
                                            <td>
                                                <a href="# ">Juan C. Cargill</a>
                                            </td>
                                            <td>
                                                Developer
                                            </td>
                                            <td>
                                                <div className="status warning">
                                                    In Progress
                                                </div>
                                            </td>
                                            <td>
                                                <div className="date">
                                                    12 Dec
                                                </div>
                                            </td>
                                            <td>
                                                <div className="achive-process">
                                                    <div className="process-percent">71%</div>
                                                    <div className="process-bar">
                                                        <div class="process" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="action">
                                                    <button className="hire">Detail</button>
                                                    {/* <button className="fire">Fire</button> */}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#54</td>
                                            <td>
                                                <div className="avatar center">
                                                    <img src="https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="" />
                                                </div>
                                            </td>
                                            <td>
                                                <a href="# ">Juan C. Cargill</a>
                                            </td>
                                            <td>
                                                Developer
                                            </td>
                                            <td>
                                                <div className="status warning">
                                                    In Progress
                                                </div>
                                            </td>
                                            <td>
                                                <div className="date">
                                                    12 Dec
                                                </div>
                                            </td>
                                            <td>
                                                <div className="achive-process">
                                                    <div className="process-percent">71%</div>
                                                    <div className="process-bar">
                                                        <div class="process" />
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="action">
                                                    <button className="hire">Detail</button>
                                                    {/* <button className="fire">Fire</button> */}
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="card-footer center">
                        <button className="grey-background">
                            <div className="icon-button">
                                <i className="fa fa-gear fa-spin"></i>
                            </div>
                            View Complete Report
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}
