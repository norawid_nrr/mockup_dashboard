import React, { Component } from 'react'

export default class IncidentList extends Component {
    render() {
        return (
            <div>
                <div className="card">
                    <div className="card-header">
                        Incident List
                    </div>
                    <div className="card-content">
                        <div className=" table-main">
                            <div className="table-wrapper">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>Level</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Solution</th>
                                            <th>Responsible by</th>
                                            <th>Modified Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2</td>
                                            <td>Problem 2</td>
                                            <td>
                                                <div className="status danger">
                                                    Critical
                                                </div>
                                            </td>
                                            <td>Criticle</td>
                                            <td>ลงมือแก้ไขปัญหา</td>
                                            <td>Task Assignment</td>
                                            <td>John Doe</td>
                                            <td>
                                                <div className="date">
                                                    18/04/2020
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Test 5</td>
                                            <td>
                                                <div className="status warning">
                                                    Minor
                                                </div>
                                            </td>
                                            <td>Window</td>
                                            <td>วางแผนแก้ไขปัญหา</td>
                                            <td>Task Assignmen</td>
                                            <td>Ruben Tillman</td>
                                            <td>
                                                <div className="date">
                                                    22/04/2020
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Incident 2</td>
                                            <td>
                                                <div className="status success">
                                                    Major
                                                </div>
                                            </td>
                                            <td>Window</td>
                                            <td>ประเมินการแก้ไขปัญหา</td>
                                            <td>Todo Task</td>
                                            <td>Elliot Huber</td>
                                            <td>
                                                <div className="date">
                                                    23/04/2020
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="card-footer center">
                        <button className="grey-background">
                            <div className="icon-button">
                                <i className="fa fa-gear fa-spin"></i>
                            </div>
                            View Complete Report
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}
