import React, { Component } from 'react'

export default class Card extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            detail: '',
            number: '',
            icon: '',
            numcolor: ''
        }
    }

    render() {
        return (
            <div>
                <div className="card">
                    <div className="group-wrapper">
                        <div className="left-content">
                            <div className="icon-wrapper">
                                <div className="icon-box-card">
                                    <i className={this.props.icon}></i>
                                </div>
                            </div>
                            <div className="group-header">
                                <div className="name">{this.props.name}</div>
                                <div className="detail ">{this.props.detail}</div>
                            </div>
                        </div>
                        <div className="group-number">
                            <div className={this.props.numcolor}>{this.props.number}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
