import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <div>
                <div className="footer-wrapper">
                    <div className="footer-copy">Copyright 2019 - DashboardPack.com</div>
                </div>
            </div>
        )
    }
}
