import React, { Component } from 'react'
import Card from './Card'

export default class CardGroup extends Component {

    render() {
        return (
            <div>
                <div className="card-group">
                    <Card name="Critical" detail="description" number="18" icon="fa fa-window-close danger" numcolor="number red" />
                    <Card name="Minor" detail="description" number="22" icon="fa fa-exclamation-triangle warning" numcolor="number yellow" />
                    <Card name="Major" detail="description" number="34" icon="fa fa-check success" numcolor="number green" />
                </div>
            </div>
        )
    }
}
