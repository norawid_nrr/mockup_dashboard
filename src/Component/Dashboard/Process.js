import React, { Component } from 'react'
import { Line } from 'react-chartjs-2'

const data = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
    datasets: [
        {
            label: "First dataset",
            data: [33, 53, 85, 41, 44, 65],
            fill: true,
            backgroundColor: "rgba(75,192,192,0.2)",
            borderColor: "rgba(75,192,192,1)"
        },
        {
            label: "Second dataset",
            data: [33, 25, 35, 51, 54, 76],
            fill: false,
            borderColor: "#742774"
        }
    ]
};

export default class Process extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-header">
                    Technical Support
                </div>
                <div className="card-content">
                    <Line data={data} />
                </div>
                <div className="card-footer">
                    <div className="process-wrapper">
                        <div className="footer-header">
                            <div className="left-header">
                                <div className="order">Total Orders</div>
                                <div className="exp">Last year expenses</div>
                            </div>
                            <div className="right-header">
                                <div className="number-process">$ 1896</div>
                            </div>
                        </div>
                        <div className="footer-content">
                            <div className="growth-process">
                                <div className="process-bar">
                                    <div class="process" />
                                </div>
                            </div>
                            <div className="text-process">
                                <div className="growth-name">YoY Growth</div>
                                <div className="growth-max">100%</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}
