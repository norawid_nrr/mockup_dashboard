import React, { Component } from 'react'

export default class TaskList extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-header">
                    <h1>Portfolio Performance </h1>
                </div>
                <div className="card-content">
                    <div className="task-wrapper">
                        <div className="task-list">
                            <div className="task-item">
                                <botton type="checkbox" className="task-checkbox"></botton>
                                <div className="task-name">
                                    <div className="task-header">
                                        <div className="name">Wash the car</div>
                                        <div className="reject">Rejected</div>
                                    </div>
                                    <div className="task-deatil">
                                        <div className="written">Written by Bob</div>
                                    </div>
                                </div>
                                <div className="task-action">
                                    <div className="done"></div>
                                    <div className="delete"></div>
                                </div>
                            </div>
                            <div className="task-item">
                                <botton type="checkbox" className="task-checkbox"></botton>
                                <div className="task-name">
                                    <div className="task-header">
                                        <div className="name">Task with dropdown menu</div>
                                    </div>
                                    <div className="task-deatil">
                                        <div className="deatil">By Johnny</div>
                                        <div className="new">New</div>
                                    </div>
                                </div>
                                <div className="task-action">
                                    <div className="done"></div>
                                    <div className="delete"></div>
                                </div>
                            </div>
                            <div className="task-item">
                                <botton type="checkbox" className="task-checkbox"></botton>
                                <div className="task-name">
                                    <div className="task-header">
                                        <div className="name">Badge on the right task</div>
                                    </div>
                                    <div className="task-deatil">
                                        <div className="deatil">This task has show on hover actions!</div>
                                    </div>
                                </div>
                                <div className="task-action">
                                    <div className="done"></div>
                                    <div className="last-task">Latest Task</div>
                                </div>
                            </div>
                            <div className="task-item">
                                <botton type="checkbox" className="task-checkbox"></botton>
                                <div className="task-ava">
                                    <img src="" alt="" />
                                </div>
                                <div className="task-name">
                                    <div className="task-header">
                                        <div className="name">Go grocery shopping</div>
                                    </div>
                                    <div className="task-deatil">
                                        <div className="deatil">A short description for this todo item</div>
                                    </div>
                                </div>
                                <div className="task-action">
                                    <div className="done"></div>
                                    <div className="delete"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-footer">
                    <a href="# " alt="">
                        Cancel
                    </a>
                    <button>
                        Add Task
                    </button>
                </div>
            </div>
        )
    }
}
