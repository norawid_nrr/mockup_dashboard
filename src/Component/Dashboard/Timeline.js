import React, { Component } from 'react'

export default class Timeline extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-header">
                    Timeline Example
                </div>
                <div className="card-content">
                    <div className="timeline-wrapper">
                        <div className="list-timeline">
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    All Hands Meeting
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    Yet another one, at 15:00 PM
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    Build the production release
                                    <div className="new">new</div>
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    Something not important
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    All Hands Meeting
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    Yet another one, at 15:00 PM
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    Build the production release
                                    <div className="new">new</div>
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    Something not important
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    Build the production release
                                    <div className="new">new</div>
                                </div>
                            </div>
                            <div className="timeline-item">
                                <span className="timeline-dot" />
                                <div className="timeline-content">
                                    Something not important
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-footer center">
                    <button>
                        View All Messages
                    </button>
                </div>
            </div>
        )
    }
}
