import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

export default function MenuConfig(props) {
    const classes = useStyles();

    function handleChange(event) {
        // Here, we invoke the callback with the new value
        props.onChange(props.name, event.target.value);
        console.log('name', event.target.name)

    }

    return (
        <div>

            <input
                placeholder="Main Menu"
                required="required"
                name="mainTitle"
                value={props.mainTitle}
                onChange={handleChange}
            />
            <input
                placeholder="Sub Menu"
                required="required"
                name={props.subTitle}
                value={props.subTitle}
                onChange={handleChange}
            />
        </div>
    );
}